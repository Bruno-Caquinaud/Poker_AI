#ifndef CIRCLE_HEADER
#define CIRCLE_HEADER

#include <opencv2/opencv.hpp>

using namespace cv;

class Circle
{
  private:

  float x;
  float y;
  float radius;

  public :

  Circle();
  Circle(Vec3f circle);
  Circle(float x, float y, float radius);
  float get_x();
  float get_y();
  float get_radius();
  void set_x(float x);
  void set_y(float x);
  void set_radius(float radius);
  static bool order_by_y(const Circle & circle1, const Circle & circle2);
  static bool order_by_x(const Circle & circle1, const Circle & circle2);
  static bool order_by_radius(const Circle & circle1, const Circle & circle2);
};

#endif
