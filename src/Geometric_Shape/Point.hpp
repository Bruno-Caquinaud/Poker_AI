#ifndef POINT_SHAPE_HEADER
#define POINT_SHAPE_HEADER

#include <opencv2/opencv.hpp>

using namespace cv;

bool order_point_by_x(const Point & point1, const Point & point2);
bool order_point_by_y(const Point & point1, const Point & point2);

#endif
