#include "Point.hpp"

bool order_point_by_x(const Point & point1, const Point & point2)
{
  return point1.x < point2.x;
}

bool order_point_by_y(const Point & point1, const Point & point2)
{
  return point1.y < point2.y;
}
