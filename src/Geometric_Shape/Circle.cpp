#include "Circle.hpp"

Circle::Circle()
{
  x = y = radius = 0;
}

Circle::Circle(Vec3f circle)
{
  x = circle[0];
  y = circle[1];
  radius = circle[2];
}

Circle::Circle(float x, float y, float radius)
{
  this->x = x;
  this->y = y;
  this->radius = radius;
}

float Circle::get_x()
{
  return x;
}

float Circle::get_y()
{
  return y;
}

float Circle::get_radius()
{
  return radius;
}

void Circle::set_x(float x)
{
  this->x = x;
}

void Circle::set_y(float y)
{
  this->y = y;
}

void Circle::set_radius(float radius)
{
  this->radius = radius;
}

bool Circle::order_by_x(const Circle & circle1, const Circle & circle2)
{
  return circle1.x < circle2.x;
}

bool Circle::order_by_y(const Circle & circle1, const Circle & circle2)
{
  return circle1.y < circle2.y;
}

bool Circle::order_by_radius(const Circle & circle1, const Circle & circle2)
{
  return circle1.radius < circle2.radius;
}
