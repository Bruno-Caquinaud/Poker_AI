#include "Poker_Star_Identifier.hpp"

void Poker_Star_Identifier::create_area_from_circle_category( vector<vector<Circle>> & circle_category, vector<Rect> & area_group)
{
  for(vector<Circle> group_circle : circle_category)
  {
    sort(group_circle.begin(), group_circle.end(), Circle::order_by_x);

    for(int i = 0; i+1 < group_circle.size(); i+=2)
    {
      Rect position_zone;

      position_zone.x = group_circle.at(i).get_x();
      position_zone.y = group_circle.at(i).get_y() -  group_circle.at(i).get_radius() + OFFSET_POSITION;
      position_zone.height = group_circle.at(i).get_radius() * 2 - OFFSET_POSITION;
      position_zone.width = group_circle.at(i+1).get_x() - group_circle.at(i).get_x();
      area_group.push_back(position_zone);
    }
  }
}

void Poker_Star_Identifier::generate_circle_category(vector<vector<Circle>> & circle_category_group, vector<Circle> & circles)
{
  for(Circle & circle : circles)
  {
    bool inserted = false;

    for(vector<Circle> & circle_category : circle_category_group)
    {
      bool same_horizon;
      float y_category, y_circle;

      y_category = circle_category.at(0).get_y();
      y_circle = circle.get_y();
      same_horizon = abs(y_category - y_circle) < 10;

      if(same_horizon)
      {
        inserted = true;
        circle_category.push_back(circle);
      }
    }

    if(!inserted)
    {
      vector<Circle> circle_category;

      circle_category.push_back(circle);
      circle_category_group.push_back(circle_category);
    }
  }
}

void Poker_Star_Identifier::player_position_detector(Mat & image, vector<Rect> & area)
{
    vector<Vec3f> circles_vec;
    vector<Circle> circles;
    vector<vector<Circle>> circle_category;

    HoughCircles(image, circles_vec, HOUGH_GRADIENT, 1, 50, 100, 30, 10, 30 );

    for(Vec3f circle : circles_vec)
    {
      circles.push_back(Circle(circle));
    }

    generate_circle_category(circle_category, circles);
    create_area_from_circle_category(circle_category, area);
}

void Poker_Star_Identifier::player_position_identifier(Mat & image, vector<Rect> & area)
{
  Mat filtered_image;

  Poker_Star_Filter::player_position_filter(image, filtered_image);
  player_position_detector(filtered_image, area);
}

void Poker_Star_Identifier::generate_area_card(Mat & image1, Mat & image, vector<Rect> & area, int nb_card)
{
  vector<vector<Point>> contours;
  vector<Vec4i> hierarchy;
  multimap<float, Rect, greater<float>> area_contour;

  findContours(image, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE );

  cout << "nombre de contour :" << contours.size() << endl;

  for(vector<Point> & polygone : contours)
  {
    float current_area;

    //approxPolyDP(polygone, polygone, 0.1*arcLength(polygone, true), true);
    current_area = contourArea(polygone);
    area_contour.emplace(current_area, boundingRect(polygone));
  }

  for(multimap<float, Rect>::iterator it = area_contour.begin(); it != area_contour.end() && distance(area_contour.begin(), it) < nb_card; it++)
  {
    area.push_back((*it).second);
  }

  /*for( Rect rect : area)
  {
    rectangle(image1, rect, Scalar(255, 0, 0));
  }

  imshow("filtered image", image1);
  waitKey(0);*/
}

void Poker_Star_Identifier::player_card_identifier(Mat & image, vector<Rect> & area, int nb_card)
{
  Mat output;

  Poker_Star_Filter::player_card_filter(image, output);
  imshow("filtered image", output);
  waitKey(0);
  generate_area_card(image, output, area, nb_card);
}
