#include "Poker_Star_Analyzer.hpp"

Poker_Star_Analyzer::Poker_Star_Analyzer():Analyseur_Interface_Graphique()
{

}

Poker_Star_Analyzer::Poker_Star_Analyzer(string image_path):Analyseur_Interface_Graphique(image_path)
{
  first_analyze = false;
}

Poker_Star_Analyzer::Poker_Star_Analyzer(Mat & image):Analyseur_Interface_Graphique(image)
{
  first_analyze = false;
}

void Poker_Star_Analyzer::analyze()
{
  vector<Rect> area;
  vector<vector<string>> values;

  if(image.data != NULL)
  {
    image = image(Rect((int)Crop::X_BASE, (int)Crop::Y_BASE, (int)Crop::WIDTH, (int)Crop::HEIGHT));

    if(!first_analyze)
    {
      first_analyze = true;
      extract_zone(image, Poker_Star_Identifier::player_position_identifier, area);
    }

    extract_value(area, values);
  }
}

void Poker_Star_Analyzer::extract_zone(Mat & input, function<void(Mat &, vector<Rect> &)> identification_function, vector<Rect> & area_group)
{
  identification_function(input, area_group);
}

void Poker_Star_Analyzer::extract_value(vector<Rect> & zone_group, vector<vector<string>> & values)
{
  for(Rect zone : zone_group)
  {
    Rect name_zone, money_zone, card_zone;
    string name, money, card;
    vector<string> player;

    name_zone = money_zone = card_zone = zone;
    name_zone.height /=2;
    money_zone.height /=2;
    card_zone.height /=2;
    money_zone.y += money_zone.height;
    card_zone.y -= card_zone.height;

    extract_name(name, name_zone);
    extract_money(money, money_zone);
    extract_card(card, card_zone);
    player.push_back(name);
    player.push_back(money);
    player.push_back(card);
    values.push_back(player);
  }
}

void Poker_Star_Analyzer::extract_name(string & name, Rect & zone)
{
  Mat filtered_image, input;

  if(image.data != NULL)
  {
    input = image(zone);
    Poker_Star_Filter::player_name_filter( input, filtered_image);
    imshow("Name", filtered_image);
    waitKey(0);
    extract_text(filtered_image, name);
  }
}

void Poker_Star_Analyzer::extract_money(string & money, Rect & zone)
{
  Mat filtered_image, input;

  if(image.data != NULL)
  {
    input = image(zone);
    Poker_Star_Filter::player_money_filter(input, filtered_image);
    imshow("Money", filtered_image);
    waitKey(0);
    extract_text(filtered_image, money);
  }
}

void Poker_Star_Analyzer::extract_card(string & cards_value, Rect & zone, int nb_card)
{
  if(image.data != NULL)
  {
    Mat input;
    vector<Rect> area;

    input = image(zone);
    //extract_zone(input, Poker_Star_Identifier::player_card_identifier, area);
    Poker_Star_Identifier::player_card_identifier(input, area, nb_card);
    extract_cards_value(input, area, cards_value);

    cout << "Cards Value : " << cards_value << ", taille : " << cards_value.size() << endl;
  }
}

void Poker_Star_Analyzer::extract_cards_value(Mat & image, vector<Rect> & area, string & cards_value)
{
  if(area.size() == 0)
  {
    cards_value.append("No card\0");
  }
  else
  {
    for(Rect card_position : area)
    {
      int card_area = card_position.width * card_position.height;

      cout << "Card area : " << card_area << endl;

      if(card_area <= 1.1 * HIDDEN_CARD_AREA && card_area >= 0.9 * HIDDEN_CARD_AREA)
      {
        cards_value.append("Unknow cards\0");
      }
      else if(card_area <= 1.1 * SHOW_CARD_AREA_PLAYER && card_area >= 0.9 * SHOW_CARD_AREA_PLAYER)
      {
        Rect value_area, symbol_area;
        Mat value_im, symbol_im;

        value_area = symbol_area = card_position;
        value_area.height *= 0.6;
        value_area.width *= 0.4;
        symbol_area.y += value_area.height;
        symbol_area.height *= 0.4;
        symbol_area.width *= 0.4;

        value_im = image(value_area);
        symbol_im = image(symbol_area);

        Poker_Star_Filter::player_card_value_filter(value_im, value_im);
        Poker_Star_Filter::player_card_symb_filter(symbol_im, symbol_im);
        extract_text(value_im, cards_value);
        extract_symb(symbol_im, cards_value);
        cards_value.erase(remove_if(cards_value.begin(), cards_value.end(), [](unsigned char x){return isspace(x);}), cards_value.end());
      }
      else if (card_area <= 1.1 * SHOW_CARD_AREA_TABLE && card_area >= 0.9 * SHOW_CARD_AREA_TABLE)
      {
        Rect value_area, symbol_area;
        Mat value_im, symbol_im;

        value_area = symbol_area = card_position;
        value_area.height *= 0.3;
        value_area.width *= 0.35;
        symbol_area.y += value_area.height;
        symbol_area.height *= 0.25;
        symbol_area.width *= 0.35;

        value_im = image(value_area);
        symbol_im = image(symbol_area);

        Poker_Star_Filter::player_card_value_filter(value_im, value_im);
        Poker_Star_Filter::player_card_symb_filter(symbol_im, symbol_im);
        extract_text(value_im, cards_value);
        extract_symb(symbol_im, cards_value);
        cards_value.erase(remove_if(cards_value.begin(), cards_value.end(), [](unsigned char x){return isspace(x);}), cards_value.end());
      }
    }
  }
}

void Poker_Star_Analyzer::extract_symb(Mat & image, string & symbol)
{
  if(image.data != NULL)
  {
    pair<int, string> best_case(256, "");
    map<string, string> template_list = {{TEMPLATE_COEUR, "H"}, {TEMPLATE_CARREAU, "D"}, {TEMPLATE_PIQUE, "S"}, {TEMPLATE_TREFLE, "C"}};

    for( pair<string, string> maxval_template : template_list)
    {
      double minVal, maxVal;
      Point minLoc, maxLoc;
      Mat result, template_im, diff;
      Rect symb_zone;
      Scalar s;

      template_im = imread(maxval_template.first, IMREAD_COLOR);
      Poker_Star_Filter::player_card_symb_filter(template_im, template_im);

      matchTemplate( image, template_im, result, TM_CCOEFF);
      minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

      symb_zone.x = maxLoc.x;
      symb_zone.y = maxLoc.y;
      symb_zone.width = template_im.cols;
      symb_zone.height = template_im.rows;
      result = image(symb_zone);
      absdiff(result, template_im, diff);
      s = mean(diff);

      if(s[0] < best_case.first)
      {
          best_case.first = s[0];
          best_case.second = maxval_template.second;
      }
    }

    cout << "Symbol : " << best_case.second << ", size : " << best_case.second.size()<< endl;
    symbol.append(best_case.second);
  }
}
