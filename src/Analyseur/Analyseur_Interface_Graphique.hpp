#ifndef ANALYZEUR_INTERFACE_GRAPHIQUE_HEADER
#define ANALYZEUR_INTERFACE_GRAPHIQUE_HEADER

#include <vector>
#include <string>
#include <cmath>
#include <functional>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <opencv2/opencv.hpp>
#include "../Interface graphique/UI_Element.hpp"
#include "../Geometric_Shape/Circle.hpp"

using namespace std;
using namespace cv;

class Analyseur_Interface_Graphique
{
  protected :

  Mat image;

  public :

  Analyseur_Interface_Graphique();
  Analyseur_Interface_Graphique(string & image_path);
  Analyseur_Interface_Graphique(Mat & image);
  Mat & get_image();
  void set_image(Mat & image);
  virtual void analyze() = 0;
  void extract_text(Mat & input, string & text);
  Mat pixToMat(Pix *pix);
};

#endif
