#ifndef POKER_STAR_ANALYSER_HEADER
#define POKER_STAR_ANALYSER_HEADER

#include <vector>
#include <string>
#include <utility>
#include <map>
#include <opencv2/opencv.hpp>
#include "Analyseur_Interface_Graphique.hpp"
#include "Poker_Star_Identifier.hpp"
#include "../Interface graphique/Poker_Star_UI.hpp"

#define TEMPLATE_COEUR "coeur.jpg"
#define TEMPLATE_CARREAU "carreau.jpg"
#define TEMPLATE_PIQUE "pique.jpg"
#define TEMPLATE_TREFLE "trefle.jpg"
#define HIDDEN_CARD_AREA 715
#define SHOW_CARD_AREA_PLAYER 1360
#define SHOW_CARD_AREA_TABLE 2550

using namespace std;
using namespace cv;
using namespace Poker_Star_UI;

class Poker_Star_Analyzer : public Analyseur_Interface_Graphique
{
  private:
  bool first_analyze;

  public :
  Poker_Star_Analyzer();
  Poker_Star_Analyzer(string image_path);
  Poker_Star_Analyzer(Mat & image);
  void analyze() override;
  void extract_symb(Mat & image, string & symb);
  void extract_name(string & name, Rect & zone);
  void extract_money(string & money, Rect & zone);
  void extract_card(string & card, Rect & zone, int nb_card = 5);
  void extract_cards_value(Mat & image, vector<Rect> & area, string & cards_value);
  void extract_value(vector<Rect> & area, vector<vector<string>> & values);
  void extract_zone(Mat & image, function<void(Mat & , vector<Rect> &)> identification_process, vector<Rect> & area);
};

#endif
