#ifndef POKER_STAR_FILTER_HEADER
#define POKER_STAR_FILTER_HEADER

#include <opencv2/opencv.hpp>

#define THRESHOLD_PLAYER_NAME 125 // 125 ou 145
#define MAX_VAL_PLAYER_NAME 255
#define THRESHOLD_PLAYER_MONEY 80 // 125 ou 145
#define MAX_VAL_PLAYER_MONEY 255
#define THRESHOLD_PLAYER_CARD 200 // 125 ou 145
#define MAX_VAL_PLAYER_CARD 255
#define THRESHOLD_PLAYER_CARD_SYMB 125 // 125 ou 145
#define MAX_VAL_PLAYER_CARD_SYMB 255
#define THRESHOLD_PLAYER_CARD_VALUE 200 // 125 ou 145
#define MAX_VAL_PLAYER_CARD_VALUE 255

using namespace cv;

class Poker_Star_Filter
{
  public :
  static void player_position_filter(Mat & input, Mat & output);
  static void player_name_filter(Mat & image, Mat & output);
  static void player_money_filter(Mat & image, Mat & output);
  static void player_card_filter(Mat & image, Mat & output);
  static void player_card_value_filter(Mat & image, Mat & output);
  static void player_card_symb_filter(Mat & image, Mat & output);
};

#endif
