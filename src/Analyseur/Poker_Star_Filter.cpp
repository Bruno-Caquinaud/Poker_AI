#include "Poker_Star_Filter.hpp"

void Poker_Star_Filter::player_position_filter(Mat & input, Mat & output)
{
  Mat grad_x, grad_y, abs_grad_x, abs_grad_y;
  int ksize, scale, delta, ddepth;

  ksize = 3;
  scale = 3;
  delta = 3;
  ddepth = CV_16S;

  cvtColor(input, output, COLOR_BGR2GRAY);
  Sobel(output, grad_x, ddepth, 1, 0, ksize, scale, delta, BORDER_DEFAULT);
  Sobel(output, grad_y, ddepth, 0, 1, ksize, scale, delta, BORDER_DEFAULT);
  convertScaleAbs(grad_x, abs_grad_x);
  convertScaleAbs(grad_y, abs_grad_y);
  addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, output);
}

void Poker_Star_Filter::player_name_filter(Mat & input, Mat & output)
{
  cvtColor( input, output, COLOR_BGR2GRAY);
  threshold( output, output, THRESHOLD_PLAYER_NAME, MAX_VAL_PLAYER_NAME, THRESH_TOZERO);
}

void Poker_Star_Filter::player_money_filter(Mat & input, Mat & output)
{
  cvtColor( input, output, COLOR_BGR2GRAY);
  threshold( output, output, THRESHOLD_PLAYER_MONEY, MAX_VAL_PLAYER_MONEY, THRESH_BINARY_INV);
}

void Poker_Star_Filter::player_card_filter(Mat & input, Mat & output)
{
  cvtColor( input, output, COLOR_BGR2GRAY);
  threshold( output, output, THRESHOLD_PLAYER_CARD, MAX_VAL_PLAYER_CARD, THRESH_BINARY);

  //Mat element1 = getStructuringElement( MORPH_RECT, Size(1, 3), Point( 0, 1));
  //morphologyEx( output, output, MORPH_CLOSE, element1);

  /*for(int i = 3; i < 7; i+=2)
  {
    Mat element = getStructuringElement( MORPH_RECT, Size(1, i), Point( 0, i/2));
    Mat element1 = getStructuringElement( MORPH_ELLIPSE, Size(i, i), Point( i/2, i/2));

    morphologyEx( output, output, MORPH_CLOSE, element);
    morphologyEx( output, output, MORPH_CLOSE, element1);
    Mat element1 = getStructuringElement( MORPH_RECT, Size(i, 1), Point( i/2, 0));
    morphologyEx( output, output, MORPH_CLOSE, element1);
    imshow("Image", output);
    waitKey(0);
  }*/
}

void Poker_Star_Filter::player_card_value_filter(Mat & input, Mat & output)
{
  cvtColor( input, output, COLOR_BGR2GRAY);
  threshold( output, output, THRESHOLD_PLAYER_CARD_VALUE, MAX_VAL_PLAYER_CARD_VALUE, THRESH_BINARY);
  //threshold( output, output, THRESHOLD_PLAYER_CARD_VALUE, MAX_VAL_PLAYER_CARD_VALUE, THRESH_BINARY_INV);
  //Mat element = getStructuringElement( MORPH_RECT, Size( 3, 3), Point( 1, 1));
  //morphologyEx( output, output, MORPH_CLOSE, element);
}

void Poker_Star_Filter::player_card_symb_filter(Mat & input, Mat & output)
{
  cvtColor( input, output, COLOR_BGR2GRAY);
  threshold( output, output, THRESHOLD_PLAYER_CARD_SYMB, MAX_VAL_PLAYER_CARD_SYMB, THRESH_BINARY);
}
