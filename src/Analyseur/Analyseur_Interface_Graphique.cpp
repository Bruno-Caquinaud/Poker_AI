#include "Analyseur_Interface_Graphique.hpp"

Analyseur_Interface_Graphique::Analyseur_Interface_Graphique()
{
}

Analyseur_Interface_Graphique::Analyseur_Interface_Graphique(string & image_path)
{
  image = imread(image_path, IMREAD_COLOR);
}

Analyseur_Interface_Graphique::Analyseur_Interface_Graphique(Mat & image)
{
  this->image = image;
}

Mat & Analyseur_Interface_Graphique::get_image()
{
  return image;
}

void Analyseur_Interface_Graphique::set_image(Mat & image)
{
  this->image = image;
}

void Analyseur_Interface_Graphique::extract_text(Mat & input, string & text)
{
  if(image.data != NULL)
  {
    tesseract::TessBaseAPI *ocr;
    Mat imt;
    Pix * pix1;

    ocr = new tesseract::TessBaseAPI();
    ocr->Init(NULL, "eng", tesseract::OEM_LSTM_ONLY);
    ocr->SetPageSegMode(tesseract::PSM_SINGLE_CHAR);
    ocr->SetImage(input.data, input.cols, input.rows, 1, input.step);
    text.append(string(ocr->GetUTF8Text()));
    cout << "Text : " << string(ocr->GetUTF8Text()) << " , size : " << string(ocr->GetUTF8Text()).size() << endl;
    ocr->End();
  }
}

Mat Analyseur_Interface_Graphique::pixToMat(Pix *pix)
{
    int width = pixGetWidth(pix);
    int height = pixGetHeight(pix);
    int depth = pixGetDepth(pix);

    Mat mat(Size(width, height), depth == 8 ? CV_8UC1 : CV_8UC3);

    for (uint32_t y = 0; y < height; ++y) {
        for (uint32_t x = 0; x < width; ++x) {
            if (depth == 8) {
                l_uint32 val;
                pixGetPixel(pix, x, y, &val);
                mat.at<uchar>(Point(x, y)) = static_cast<uchar>(255 * val);
            } else {
                l_int32 r, g, b;
                pixGetRGBPixel(pix, x, y, &r, &g, &b);

                Vec3b color(b, g, r);
                mat.at<Vec3b>(cv::Point(x, y)) = color;
            }
        }
    }

    return mat;
}
