#ifndef POKER_STAR_IDENTIFIER_HEADER
#define POKER_STAR_IDENTIFIER_HEADER

#include <iterator>
#include <functional>
#include <map>
#include <vector>
#include <opencv2/opencv.hpp>
#include "Poker_Star_Filter.hpp"
#include "../Geometric_Shape/Circle.hpp"
#include "../Geometric_Shape/Point.hpp"

#define OFFSET_POSITION 5
#define WIDTH_CARD 15
#define HEIGHT_CARD 30

using namespace std;
using namespace cv;

class Poker_Star_Identifier
{
  private :

  static void player_position_detector( Mat & image, vector<Rect> & area);
  static void generate_circle_category(vector<vector<Circle>> & circle_category, vector<Circle> & circles);
  static void create_area_from_circle_category(vector<vector<Circle>> & cirlce_category, vector<Rect> & area);
  static void player_card_detector(Mat & image1, Mat & image, vector<Rect> & area);
  static void generate_area_card(Mat & image1, Mat & image, vector<Rect> & points, int nb_card);

  public :

  static void player_position_identifier(Mat & image, vector<Rect> & area);
  static void player_card_identifier(Mat & image, vector<Rect> & area, int nb_card);
};

#endif
