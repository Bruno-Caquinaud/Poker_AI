#ifndef CARD_HEADER
#define CARD_HEADER

#include "Poker_Features.hpp"

using namespace std;
using namespace Poker_Features;

class Card
{
  private:
  Figure figure;
  Color color;

  public:
  Card();
  Card(Figure figure, Color color);
  Figure get_figure();
  Color get_color();
  bool operator<(const Card & card);
};

#endif
