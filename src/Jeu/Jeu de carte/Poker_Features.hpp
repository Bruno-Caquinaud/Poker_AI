#ifndef POKER_FEATURES_HEADER
#define POKER_FEATURES_HEADER

namespace Poker_Features
{
  enum class Poker_Step : char { None, Preflop, Flop, Turn, River, Show};
  enum class Poker_Action : char { None, Small_blind, Big_blind, Fold, Check, Call, Bet, Raise};
  enum class Color : char { None, Diamond, Heart, Spade, Club};
  enum class Figure : char { None, As, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King};
  enum class Combinaison_Points : char { None, Pair, Double_Pair, Brelan, Quinte, Color, Full, Carre, Quinte_Flush};
}

#endif
