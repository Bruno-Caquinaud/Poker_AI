#include "Poker.hpp"

Poker::Poker()
{
  game_id++;
  date = "today";
  pot = 0;
}

vector<Player> Poker::get_players_list()
{
  return players_list;
}

void Poker::add_player(Player player)
{
  players_list.push_back(player);
}

void Poker::add_bank_card(Card card)
{
  bank_cards.push_back(card);
}

void Poker::set_pot(int pot)
{
  this->pot = pot;
}

void Poker::save_data()
{

}
