#include <string>
#include <vector>
#include "Player.hpp"

using namespace std;

class Poker
{
  private:
  static unsigned int game_id;
  string date;
  vector<Player> players_list;
  vector<Card> bank_cards;
  int pot;

  public:
  Poker();
  vector<Player> get_players_list();
  void add_player(Player player);
  void add_bank_card(Card card);
  void set_pot(int pot);
  void save_data();
};
