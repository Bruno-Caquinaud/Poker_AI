#include "Combinaison.hpp"

Combinaison::Combinaison()
{
    name = "";
    point = 0;
    combinaison = nullptr;
}

Combinaison::Combinaison(string name, int point, bool (*combinaison)(vector<Card>))
{
  this->name = name;
  this->point = point;
  this->combinaison = combinaison;
}

bool Combinaison::is_validate_combinaison(vector<Card> jeu_de_cartes)
{
  bool validate = false;

  if(combinaison != nullptr)
  {
    validate = combinaison(jeu_de_cartes);
  }

  return validate;
}

int Combinaison::get_point()
{
  return point;
}
