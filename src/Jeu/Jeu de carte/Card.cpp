#include "Card.hpp"

Card::Card()
{
  figure = Figure::None;
  color = Color::None;
}

Card::Card(Figure figure, Color color)
{
  this->figure = figure;
  this->color = color;
}

Figure Card::get_figure()
{
  return this->figure;
}

Color Card::get_color()
{
  return this->color;
}

bool Card::operator<(const Card & card)
{
  return (char)card.figure < (char)this->figure;
}
