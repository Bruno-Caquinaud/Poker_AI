#include "Player.hpp"

Player::Player()
{
  name = "";
  money = 0;
  nationality = "";
}

Player::Player(string name, int money)
{
  this->name = name;
  this->money = money;
  this->nationality = "";
}

Player::Player(string name, int money, string nationality)
{
  this->name = name;
  this->money = money;
  this->nationality = nationality;
}

string Player::get_name()
{
  return this->name;
}

int Player::get_money()
{
  return this->money;
}

vector<Card> Player::get_hand()
{
  return hand;
}

string Player::get_nationality()
{
  return this->nationality;
}

void Player::set_name(string name)
{
  this->name = name;
}

void Player::set_money(int money)
{
  this->money = money;
}

void Player::set_nationality(string nationality)
{
  this->nationality = nationality;
}

void Player::add_action(Poker_Step step, Poker_Action action)
{
  if(!((char)step < (char)Poker_Step::Preflop || (char)step > (char)Poker_Step::Show)
    && !((char)action < (char)Poker_Action::None || (char)action > (char)Poker_Action::Raise))
  {
    action_per_step.emplace(step, action);
  }
}


void Player::get_identity()
{

}

void Player::get_list_action_for_step(Poker_Step step)
{

}
