#ifndef COMBINAISON_HEADER
#define COMBINAISON_HEADER

#include <string>
#include <vector>
#include "Card.hpp"

using namespace std;

class Combinaison
{
  private:
  string name;
  int point;
  bool (*combinaison)(vector<Card>);

  public:
  Combinaison();
  Combinaison(string name, int point, bool (*combinaison)(vector<Card>));
  bool is_validate_combinaison(vector<Card> jeu_de_cartes);
  int get_point();
};

#endif
