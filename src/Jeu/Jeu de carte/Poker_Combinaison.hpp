#ifndef POKDER_COMBINAISONS_HEADER
#define POKDER_COMBINAISONS_HEADER

#include <algorithm>
#include <vector>
#include "Card.hpp"

using namespace std;
using namespace Poker_Features;

#define NB_PAIR_CARD 2
#define NB_DOUBLE_PAIR_CARD 4
#define NB_BRELAN_CARD 3
#define NB_QUINTE_CARD 5
#define NB_COLOR_CARD 5
#define NB_FULL_CARD 5
#define NB_CARRE_CARD 4

bool combinaison_plus_haute_carte(vector<Card> jeu_de_cartes);
bool combinaison_pair(vector<Card> jeu_de_cartes);
bool combinaison_double_pair(vector<Card> jeu_de_cartes);
bool combinaison_brelan(vector<Card> jeu_de_cartes);
bool combinaison_quinte(vector<Card> jeu_de_cartes);
bool combinaison_couleur(vector<Card> jeu_de_cartes);
bool combinaison_full(vector<Card> jeu_de_cartes);
bool combinaison_carre(vector<Card> jeu_de_cartes);
bool combinaison_quinte_flush(vector<Card> jeu_de_cartes);

#endif
