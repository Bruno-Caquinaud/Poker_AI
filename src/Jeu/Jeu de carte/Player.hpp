#ifndef PLAYER_HEADER
#define PLAYER_HEADER

#include <string>
#include <vector>
#include <map>
#include "Card.hpp"
#include "Poker_Features.hpp"

using namespace std;
using namespace Poker_Features;

class Player
{
  private:
  string name;
  int money;
  string nationality;
  vector<Card> hand;
  map<Poker_Step, Poker_Action> action_per_step;

  public:

  Player();
  Player(string name, int money);
  Player(string name, int money, string nationality);
  string get_name();
  int get_money();
  string get_nationality();
  vector<Card> get_hand();
  void set_name(string name);
  void set_money(int money);
  void set_nationality(string nationality);
  void add_hand_card(Card carte);
  void add_action(Poker_Step step, Poker_Action action);
  void get_identity();// A définir
  void get_list_action_for_step(Poker_Step step); // A définir
};

#endif
