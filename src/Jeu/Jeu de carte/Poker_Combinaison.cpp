#include "Poker_Combinaison.hpp"

bool combinaison_pair(vector<Card> jeu_de_cartes)
{
  bool validate = false;
  vector<Card>::iterator it_card_set = jeu_de_cartes.begin();

  if(jeu_de_cartes.size() >= NB_PAIR_CARD)
  {
    while(it_card_set != jeu_de_cartes.end() && !validate)
    {
      vector<Card>::iterator it_current_card = ++it_card_set;

      while(it_current_card != jeu_de_cartes.end() && !validate)
      {
        if((*it_card_set).get_figure() == (*it_current_card).get_figure())
        {
          validate = true;
        }

        it_current_card++;
      }

      it_card_set++;
    }
  }

  return validate;
}

bool combinaison_double_pair(vector<Card> jeu_de_cartes)
{
  bool first_pair = false;
  bool second_pair = false;
  vector<Card>::iterator it_card_set = jeu_de_cartes.begin();

  if(jeu_de_cartes.size() >= NB_DOUBLE_PAIR_CARD)
  {
    while(it_card_set != jeu_de_cartes.end() && !first_pair && !second_pair)
    {
      vector<Card>::iterator it_current_card = ++it_card_set;

      while(it_current_card != jeu_de_cartes.end() && !first_pair && !second_pair)
      {
        if((*it_card_set).get_figure() == (*it_current_card).get_figure())
        {
          if(!first_pair)
          {
            first_pair = true;
          }
          else
          {
            second_pair = true;
          }
        }

        it_current_card++;
      }

      it_card_set++;
    }
  }

  return first_pair && second_pair;
}

bool combinaison_brelan(vector<Card> jeu_de_cartes)
{
  bool validate = false;
  vector<Card>::iterator it_first_card = jeu_de_cartes.begin();

  if(jeu_de_cartes.size() >= NB_BRELAN_CARD)
  {
    while(it_first_card != jeu_de_cartes.end() && !validate)
    {
      vector<Card>::iterator it_second_card = ++it_first_card;

      while(it_first_card != jeu_de_cartes.end() && !validate)
      {
        vector<Card>::iterator it_third_card = ++it_second_card;

        while(it_third_card != jeu_de_cartes.end() && !validate)
        {
          if((*it_first_card).get_figure() == (*it_second_card).get_figure()
            && (*it_first_card).get_figure() == (*it_third_card).get_figure())
          {
            validate = true;
          }

          it_third_card++;
        }

        it_second_card++;
      }

      it_first_card++;
    }
  }

  return validate;
}

bool combinaison_quinte(vector<Card> jeu_de_cartes)
{
  bool validate = false;
  vector<Card>::iterator current_card, next_card;

  if( jeu_de_cartes.size() >= NB_QUINTE_CARD)
  {
    sort(jeu_de_cartes.begin(), jeu_de_cartes.end());
    current_card = jeu_de_cartes.begin();
    next_card = current_card++;

    if((*current_card).get_figure() == Figure::As)
    {
      current_card++;
      next_card++;
    }

    while(next_card != jeu_de_cartes.end())
    {
      if((char)(*next_card).get_figure() - (char)(*current_card).get_figure() == 1)
      {
        current_card++;
        next_card++;

        if(next_card == jeu_de_cartes.end())
        {
          validate = true;
        }
      }
      else
      {
        next_card = jeu_de_cartes.end();
      }
    }
  }

  return validate;
}

bool combinaison_couleur(vector<Card> jeu_de_cartes)
{
  bool validate = false;
  vector<Card>::iterator current_card, next_card;

  if(jeu_de_cartes.size() >= NB_COLOR_CARD)
  {
    current_card = jeu_de_cartes.begin();
    next_card = current_card++;

    while(next_card != jeu_de_cartes.end())
    {
      if((*current_card).get_color() == (*next_card).get_color())
      {
        current_card++;
        next_card++;

        if(next_card == jeu_de_cartes.end())
        {
          validate = true;
        }
      }
      else
      {
        next_card = jeu_de_cartes.end();
      }
    }
  }

  return validate;
}

bool combinaison_full(vector<Card> jeu_de_cartes)
{
  bool validate = false;
  vector<Card>::iterator first_card, second_card, third_card, fourth_card, fifth_card;

  if(jeu_de_cartes.size() >= NB_FULL_CARD)
  {
    sort(jeu_de_cartes.begin(), jeu_de_cartes.end());
    first_card = jeu_de_cartes.begin();
    second_card = next(first_card);
    third_card = next(second_card);
    fourth_card = next(third_card);
    fifth_card = next(fourth_card);

    if((*first_card).get_figure() == (*second_card).get_figure()
      || (*fourth_card).get_figure() == (*fifth_card).get_figure())
    {
      if(((*first_card).get_figure() == (*third_card).get_figure()
        && (*fourth_card).get_figure() == (*fifth_card).get_figure())
      || ((*fifth_card).get_figure() == (*third_card).get_figure()
        && (*first_card).get_figure()  == (*second_card).get_figure()))
      {
          validate = true;
      }
    }
  }

  return validate;
}

bool combinaison_carre(vector<Card> jeu_de_cartes)
{
  bool validate = false;
  vector<Card>::iterator first_card, second_card, third_card, fourth_card, fifth_card;

  if(jeu_de_cartes.size() >= NB_CARRE_CARD)
  {
    sort(jeu_de_cartes.begin(), jeu_de_cartes.end());
    first_card = jeu_de_cartes.begin();
    second_card = next(first_card);
    third_card = next(second_card);
    fourth_card = next(third_card);
    fifth_card = next(fourth_card);

    if((*first_card).get_figure() == (*second_card).get_figure()
      || (*fourth_card).get_figure() == (*fifth_card).get_figure())
    {
      if(((*first_card).get_figure() == (*third_card).get_figure()
        && (*first_card).get_figure() == (*fourth_card).get_figure())
      || ((*fifth_card).get_figure() == (*third_card).get_figure()
        && (*fifth_card).get_figure()  == (*second_card).get_figure()))
        {
          validate = true;
        }
    }
  }
  
  return validate;
}

bool combinaison_quinte_flush(vector<Card> jeu_de_cartes)
{
  return combinaison_couleur(jeu_de_cartes) && combinaison_quinte(jeu_de_cartes);
}
