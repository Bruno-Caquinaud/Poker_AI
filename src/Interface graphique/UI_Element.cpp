#include "UI_Element.hpp"

UI_Element::UI_Element()
{
  name = "";
  value = "";
}

UI_Element::UI_Element(string name, Rect zone)
{
  this->name = name;
  this->zone = zone;
  value = "";
}

string UI_Element::get_name()
{
  return this->name;
}

void UI_Element::set_name(string name)
{
  this->name = name;
}

Rect_<float> UI_Element::get_zone()
{
  return this->zone;
}

void UI_Element::set_zone(Rect_<float> zone)
{
  this->zone = zone;
}

string UI_Element::get_value()
{
    return this->value;
}

void UI_Element::set_value(string value)
{
  this->value = value;
}
