#include <vector>
#include <cstring>
#include <string>
#include <iostream>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "UI_Element.hpp"

using namespace std;

class Interface_Graphique
{
  private:

  string name;
  Window * window;
  Display * display;
  vector<UI_Element> liste_element;

  public:

  Interface_Graphique();
  Interface_Graphique(const Interface_Graphique & copy);
  ~Interface_Graphique();
  Interface_Graphique & operator=(const Interface_Graphique & copy);
  string get_name();
  void set_name(string name);
  Window * get_window();
  void set_window(Window * window);
  Display * get_display();
  void set_display(Display * display);
  vector<UI_Element> * get_liste_element();
  void set_liste_element(vector<UI_Element> * liste_element);
  void search_window(Window root);
  void minimize();
  void maximize();
  XImage * get_image();
};
