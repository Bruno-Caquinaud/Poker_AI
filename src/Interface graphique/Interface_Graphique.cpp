#include "Interface_Graphique.hpp"

Interface_Graphique::Interface_Graphique()
{
  cout << "Constructor window :" << window << endl;
  name = "";
  window = nullptr;
  display = nullptr;
}

Interface_Graphique::Interface_Graphique(const Interface_Graphique & copy):Interface_Graphique()
{
  name = copy.name;

  if(copy.display!=nullptr)
  {
    this->display = XOpenDisplay(NULL);
  }

  if(copy.window != nullptr)
  {
    delete this->window;
    this->window = new Window;
    *this->window = *copy.window;
  }
}

Interface_Graphique::~Interface_Graphique()
{
  cout << "Debut Destructor" << endl;

  if(window != nullptr)
  {
    delete window;
    window = nullptr;
  }

  if(display != nullptr)
  {
    XCloseDisplay(display);
  }

  cout << "Fin Destructor" << endl;
}

Interface_Graphique & Interface_Graphique::operator=(const Interface_Graphique & copy)
{
  cout << "Debut Operaor assignement" << endl;

  if(this != &copy)
  {
    if(this->window != nullptr)
    {
      cout << "Debut Free window :" << window << endl;
      free(window);
      window = nullptr;
      cout << "Fin Free window" << endl;
    }
    if(this->display != nullptr)
    {
      cout << "Debut Free Display" << endl;
      XCloseDisplay(display);
      display = nullptr;
      cout << "Fin Free Display" << endl;
    }

    this->name = copy.name;

    if(copy.display != nullptr)
    {
      this->display = XOpenDisplay(NULL);
    }

    if(copy.window != nullptr)
    {
      this->window = (Window *) malloc(sizeof(Window));
      *this->window = *copy.window;
    }
  }
  cout << "Fin Operaor assignement" << endl;
  return *this;
}

string Interface_Graphique::get_name()
{
  return this->name;
}

void Interface_Graphique::set_name(string name)
{
  this->name = name;
}

Window * Interface_Graphique::get_window()
{
  return this->window;
}

void Interface_Graphique::set_window(Window * window)
{
  this->window = window;
}

Display * Interface_Graphique::get_display()
{
  return display;
}

void Interface_Graphique::set_display(Display * display)
{
  this->display = display;
}

vector<UI_Element> * Interface_Graphique::get_liste_element()
{
  return &liste_element;
}

void Interface_Graphique::set_liste_element(vector<UI_Element> * liste_element)
{
  this->liste_element = *liste_element;
}

void Interface_Graphique::search_window(Window root)
{
  Window parent;
  Window *children;
  unsigned int num_children = 0;
  unsigned i;
  int status = 0;
  XTextProperty tex_prop;
  char **list_return = NULL;
  int count_return = 0;

  window = nullptr;

  if(display != nullptr)
  {
    status = XGetWMName(display, root, &tex_prop);

    if(status && tex_prop.value && tex_prop.nitems)
    {
      status = XmbTextPropertyToTextList (display, &tex_prop, &list_return, &count_return);
      if(status >= Success && count_return > 0 && *list_return)
      {
        cout << "Name :" << (char*) strdup (*list_return) << endl;
        if( !strcmp(&name[0], (char*) strdup (*list_return)) )
        {
          XFreeStringList(list_return);
          window = (Window *) malloc(sizeof(Window));
          *window = root;
        }
      }
    }
    else
    {
      status = XQueryTree (display, root, &root, &parent, &children, &num_children);

      if (status == 0 || num_children == 0)
      {
        window = nullptr;
      }
      else
      {
        for (i = 0; i < num_children && window == nullptr; i++)
        {
          search_window(children[i]);
        }
      }

      XFree ((char*) children);
    }
  }
}

void Interface_Graphique::minimize()
{
  if(display != nullptr && window != nullptr)
  {
      XLowerWindow(display, *window);
  }
}

void Interface_Graphique::maximize()
{
  if(window != nullptr && display != nullptr)
  {
    XRaiseWindow(display, *window);
  }
}

XImage * Interface_Graphique::get_image()
{
  int x, y;
  XImage * image;
  XWindowAttributes window_attributes;

  image = nullptr;

  if(display != nullptr && window != nullptr)
  {
    x = y = 0;
    XGetWindowAttributes(display, *window, &window_attributes);
    image = XGetImage (display, *window, x, y, window_attributes.width, window_attributes.height, AllPlanes, ZPixmap);
  }

  return image;
}
