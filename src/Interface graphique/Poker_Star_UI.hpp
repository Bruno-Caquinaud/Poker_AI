#ifndef POKER_STAR_UI_HEADER
#define POKER_STAR_UI_HEADER

namespace Poker_Star_UI
{
  const string PLAYER_NAME = "name";
  const string PLAYER_MONEY = "money";
  const string PLAYER_CARD = "card";
  enum class UI_Size : int { WIDTH = 800, HEIGHT = 600};
  enum class Crop : int { X_BASE = 60, Y_BASE = 85, WIDTH = (int)UI_Size::WIDTH - (int)X_BASE, HEIGHT = (int)UI_Size::HEIGHT - (int)Y_BASE};
  enum class Offset : int { X_POSITION_PLAYER, Y_POSITION_PLAYER, X_NAME_PLAYER, Y_NAME_PLAYER, X_MONEY_PLAYER, Y_MONEY_PLAYER};
}

#endif
