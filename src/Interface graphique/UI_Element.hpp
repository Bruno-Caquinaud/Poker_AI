#include <string>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class UI_Element
{
  private :

  string name;
  Rect_<float> zone;
  string value;

  public :

  UI_Element();
  UI_Element(string name, Rect zone);
  string get_name();
  Rect_<float> get_zone();
  string get_value();
  void set_name(string name);
  void set_zone(Rect_<float> zone);
  void set_value(string value);
};
