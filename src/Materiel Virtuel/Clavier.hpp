#include <iostream>
#include <string>
#include <X11/Xutil.h>
#include <X11/extensions/XTest.h>

using namespace std;

class Clavier
{
  private :
  string input;
  Display * display;
  KeySym convert_key_to_keysim(char caracter);

  public :
  Clavier();
  Clavier(string input, Display * display);
  void set_input(string * input);
  string get_input();
  void set_display(Display * display);
  Display * get_display();
  void send_input();
};
