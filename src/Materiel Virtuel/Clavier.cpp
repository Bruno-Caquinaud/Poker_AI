#include "Clavier.hpp"

Clavier::Clavier()
{
  this->input = "";
  this->display = nullptr;
}

Clavier::Clavier(string input, Display * display)
{
    this->input = input;
    this->display = display;
}

string Clavier::get_input()
{
  return this->input;
}

void Clavier::set_input(string * input)
{
  this->input = *input;
}

Display * Clavier::get_display()
{
  return this->display;
}

void Clavier::set_display(Display * display)
{
  this->display = display;
}

KeySym Clavier::convert_key_to_keysim(char caracter)
{
  KeySym keysym = 0;

  switch (caracter)
  {
    case '0' :
      keysym = XK_KP_0;
      break;
    case '1' :
      keysym = XK_KP_1;
      break;
    case '2':
      keysym = XK_KP_2;
      break;
    case '3' :
      keysym = XK_KP_3;
      break;
    case '4' :
      keysym = XK_KP_4;
      break;
    case '5' :
      keysym = XK_KP_5;
      break;
    case '6' :
      keysym = XK_KP_6;
      break;
    case '7' :
      keysym = XK_KP_7;
      break;
    case '8' :
      keysym = XK_KP_8;
      break;
    case '9' :
      keysym = XK_KP_9;
      break;
    case '\n' :
      keysym = XK_KP_Enter;
      break;
    default:
      keysym = 0;
      break;
  }

  return keysym;
}

void Clavier::send_input()
{
  KeyCode keycode = 0;

  for(char key : input)
  {
    KeySym keysym = convert_key_to_keysim(key);

    keycode = XKeysymToKeycode (display, keysym);

    if (keycode != 0)
    {
      XTestGrabControl (display, True);
      XTestFakeKeyEvent (display, keycode, True, 0);
      XTestFakeKeyEvent (display, keycode, False, 0);
      XSync (display, False);
      XTestGrabControl (display, False);
    }
  }
}
