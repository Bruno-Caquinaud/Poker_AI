#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#include <unistd.h>

using namespace std;

class Souris
{
	private :
	int x;
	int y;
	Display * display;

	public:

	Souris();
	void set_display(Display * display);
	Display * get_display();
	int get_x();
	int get_y();
	void set_x(int x);
	void set_y(int y);
	void move_absolute();
	void click(int button);
};
