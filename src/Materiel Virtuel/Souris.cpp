#include "Souris.hpp"

Souris::Souris()
{
	this->x = 0;
	this->y = 0;
	this->display = nullptr;
}

Display * Souris::get_display()
{
	return this->display;
}

void Souris::set_display(Display * display)
{
	this->display = display;
}

int Souris::get_x()
{
	return this->x;
}

int Souris::get_y()
{
	return this->y;
}

void Souris::set_x(int x)
{
	this->x = x;
}

void Souris::set_y(int y)
{
	this->y = y;
}

void Souris::move_absolute()
{
	if(display != nullptr)
	{
		XEvent event;

		XQueryPointer (display, RootWindow (display, 0), &event.xbutton.root,
		 &event.xbutton.window, &event.xbutton.x_root,
		 &event.xbutton.y_root, &event.xbutton.x, &event.xbutton.y,
		 &event.xbutton.state);
		XTestFakeMotionEvent (display, 0, this->x, this->y, CurrentTime);
	}
}

void Souris::click(int button)
{
	if(display != nullptr)
	{
		XEvent event;

		XQueryPointer (display, RootWindow (display, 0), &event.xbutton.root,
		 &event.xbutton.window, &event.xbutton.x_root,
		 &event.xbutton.y_root, &event.xbutton.x, &event.xbutton.y,
		 &event.xbutton.state);
		XTestFakeButtonEvent (display, button, True,  CurrentTime);
	  XTestFakeButtonEvent (display, button, False, CurrentTime);
	}
}
