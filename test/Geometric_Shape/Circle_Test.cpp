#include "Circle_Test.hpp"

Circle_Test::Circle_Test()
{
  success = false;
}

void Circle_Test::init_jeu_entree()
{
	init_jeu_entree_test_order_by_x();
}

void Circle_Test::init_jeu_sortie()
{
	init_jeu_sortie_test_order_by_x();
}

void Circle_Test::init_jeu_entree_test_order_by_x()
{
	vector<vector<Circle>> jeu_de_donne_entree_test_order_by_x;

	for(int test_case = 0; test_case < NB_TEST_CASE_ORDER_BY_X; test_case++)
	{
    vector<Circle> circle_list;

		switch(test_case)
		{
			case 0 :
        circle_list.push_back(Circle(9, 0, 0));
        circle_list.push_back(Circle(8, 0, 0));
        circle_list.push_back(Circle(3, 0, 0));
        circle_list.push_back(Circle(2, 0, 0));
        circle_list.push_back(Circle(5, 0, 0));
        circle_list.push_back(Circle(1, 0, 0));
				jeu_de_donne_entree_test_order_by_x.push_back(circle_list);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

	jeu_entree.push_back(jeu_de_donne_entree_test_order_by_x);
}

void Circle_Test::init_jeu_sortie_test_order_by_x()
{
  vector<vector<Circle>> jeu_de_donne_sortie_test_order_by_x;

  for(int test_case = 0; test_case < NB_TEST_CASE_ORDER_BY_X; test_case++)
  {
    vector<Circle> circle_list;

    switch(test_case)
    {
      case 0 :
        circle_list.push_back(Circle(1, 0, 0));
        circle_list.push_back(Circle(2, 0, 0));
        circle_list.push_back(Circle(3, 0, 0));
        circle_list.push_back(Circle(5, 0, 0));
        circle_list.push_back(Circle(8, 0, 0));
        circle_list.push_back(Circle(9, 0, 0));
        jeu_de_donne_sortie_test_order_by_x.push_back(circle_list);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_sortie.push_back(jeu_de_donne_sortie_test_order_by_x);
}

void Circle_Test::test()
{
	init_jeu_entree();
	init_jeu_sortie();
	test_order_by_x();
	success = true;
}

void Circle_Test::test_order_by_x()
{
  vector<vector<Circle>> jeu_entree_order_by_x = jeu_entree.at(0);
  vector<vector<Circle>> jeu_sortie_order_by_x = jeu_sortie.at(0);

  for(int i = 0; i < jeu_entree_order_by_x.size(); i++)
  {
    vector<Circle> & unordered_list = jeu_entree_order_by_x.at(i);
    vector<Circle> & ordered_list = jeu_sortie_order_by_x.at(i);

    sort(unordered_list.begin(), unordered_list.end(), Circle::order_by_x);

    for(int u = 0; u < unordered_list.size(); u++)
    {
      if(unordered_list.at(u).get_x() != ordered_list.at(u).get_x())
      {
        success = false;
      }
    }
  }
}
