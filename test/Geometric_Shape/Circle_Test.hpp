#include <vector>
#include "../../src/Geometric_Shape/Circle.hpp"
#include "../Test.hpp"

#define NB_TEST_CASE_ORDER_BY_X 1

using namespace std;

class Circle_Test : public Test<vector<Circle>, vector<Circle>>
{
  private:
  virtual void init_jeu_entree() override;
  virtual void init_jeu_sortie() override;
  void init_jeu_entree_test_move();
  void init_jeu_entree_test_click();
  void init_jeu_entree_test_order_by_x();
  void init_jeu_sortie_test_order_by_x();

  public:
  Circle_Test();
  virtual void test() override;
  void test_order_by_x();
};
