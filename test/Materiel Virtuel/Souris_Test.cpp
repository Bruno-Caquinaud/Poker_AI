#include "Souris_Test.hpp"

Souris_Test::Souris_Test()
{
	success = false;
}

void Souris_Test::init_jeu_entree()
{
	init_jeu_entree_test_move();
	init_jeu_entree_test_click();
}

void Souris_Test::init_jeu_sortie()
{
	init_jeu_sortie_test_move();
	init_jeu_sortie_test_click();
}

void Souris_Test::init_jeu_entree_test_move()
{
	vector<Souris> jeu_de_donne_entree_move;

	for(int test_case = 0; test_case < NB_TEST_CASE_MOVE; test_case++)
	{
		Souris souris;
		Display *dpy;
		int x, y;

		switch(test_case)
		{
			case 0 :
				jeu_de_donne_entree_move.push_back(souris);
				break;
			case 1 :
				dpy = XOpenDisplay (NULL);
				x = X_POSITION_ICON;
				y = Y_POSITION_ICON;
				souris.set_x(x);
				souris.set_y(y);
				souris.set_display(dpy);
				jeu_de_donne_entree_move.push_back(souris);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}
	jeu_entree.push_back(jeu_de_donne_entree_move);
}

void Souris_Test::init_jeu_entree_test_click()
{
	vector<Souris> jeu_de_donne_entree_click;

	for(int test_case = 0; test_case < NB_TEST_CASE_MOVE; test_case++)
	{
		Souris souris;
		Display *dpy;

		switch (test_case)
		{
			case 0 :
				jeu_de_donne_entree_click.push_back(souris);
				break;
			case 1 :
				dpy = XOpenDisplay (NULL);
				souris.set_display(dpy);
				jeu_de_donne_entree_click.push_back(souris);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

	jeu_entree.push_back(jeu_de_donne_entree_click);
}

void Souris_Test::init_jeu_sortie_test_move()
{

}

void Souris_Test::init_jeu_sortie_test_click()
{

}

void Souris_Test::test()
{
	init_jeu_entree();
	init_jeu_sortie();
	test_move();
	test_click();
	success = true;
}

void Souris_Test::test_move()
{
	vector<Souris> jeu_entree_move = jeu_entree.at(0);

	for(int i = 0; i < jeu_entree_move.size(); i++)
	{
		Souris & souris = jeu_entree_move.at(i);

		souris.move_absolute();

		if(souris.get_display()!=NULL)
		{
			XCloseDisplay(souris.get_display());
		}
	}
}

void Souris_Test::test_click()
{
	vector<Souris> jeu_entree_click = jeu_entree.at(1);

	for(int i = 0; i < jeu_entree_click.size(); i++)
	{
		Souris & souris = jeu_entree_click.at(i);

		souris.click(LEFT_CLICK);
		if(souris.get_display()!=NULL)
		{
			XCloseDisplay(souris.get_display());
		}
	}
}
