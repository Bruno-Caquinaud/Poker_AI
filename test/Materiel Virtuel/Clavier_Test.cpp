#include "Clavier_Test.hpp"

Clavier_Test::Clavier_Test()
{
  success = false;
}

void Clavier_Test::init_jeu_entree()
{
	init_jeu_entree_test_send_input();
}

void Clavier_Test::init_jeu_sortie()
{
	init_jeu_sortie_test_send_input();
}

void Clavier_Test::init_jeu_entree_test_send_input()
{
	vector<Clavier> jeu_de_donne_entree_send_input;

	for(int test_case = 0; test_case < NB_TEST_CASE_SEND_INPUT; test_case++)
	{
		Clavier clavier;
		Display *dpy;
		string * input;

		switch(test_case)
		{
			case 0 :
				jeu_de_donne_entree_send_input.push_back(clavier);
				break;
			case 1 :
        dpy = XOpenDisplay(NULL);
        input = new string("12345\n");
        clavier.set_display(dpy);
				clavier.set_input(input);
				jeu_de_donne_entree_send_input.push_back(clavier);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

  jeu_entree.push_back(jeu_de_donne_entree_send_input);
}

void Clavier_Test::init_jeu_sortie_test_send_input()
{

}

void Clavier_Test::test()
{
  init_jeu_entree();
  init_jeu_sortie();

  test_send_input();
  success = true;
}

void Clavier_Test::test_send_input()
{
	vector<Clavier> jeu_entree_send_input = jeu_entree.at(0);

	for(int i = 0; i < jeu_entree_send_input.size(); i++)
	{
		Clavier & clavier = jeu_entree_send_input.at(i);

		clavier.send_input();
    if(clavier.get_display()!= NULL)
    {
		    XCloseDisplay(clavier.get_display());
    }
	}
}
