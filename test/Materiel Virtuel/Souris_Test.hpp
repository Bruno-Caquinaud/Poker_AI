#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <X11/Xutil.h>
#include <X11/extensions/XTest.h>
#include "../../src/Materiel Virtuel/Souris.hpp"
#include "../Test.hpp"

#define NB_TEST_CASE_MOVE 2
#define NB_TEST_CASE_CLICK 2
#define X_POSITION_ICON 100
#define Y_POSITION_ICON 100
#define LEFT_CLICK 1
using namespace std;

class Souris_Test : public Test<Souris, Souris>
{
	private :

	virtual void init_jeu_entree() override;
	virtual void init_jeu_sortie() override;
	void init_jeu_entree_test_move();
	void init_jeu_entree_test_click();
	void init_jeu_sortie_test_move();
	void init_jeu_sortie_test_click();

	public :

	Souris_Test();
	virtual void test() override;
	void test_move();
	void test_click();
};
