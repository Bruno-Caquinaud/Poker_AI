#include <iostream>
#include <vector>
#include <string>
#include <X11/Xutil.h>
#include <X11/extensions/XTest.h>
#include "../../src/Materiel Virtuel/Clavier.hpp"
#include "../Test.hpp"

#define NB_TEST_CASE_SEND_INPUT 2

using namespace std;

class Clavier_Test : public Test<Clavier, Clavier>
{
	private :

	virtual void init_jeu_entree() override;
	virtual void init_jeu_sortie() override;
	void init_jeu_entree_test_send_input();
	void init_jeu_sortie_test_send_input();

	public :

	Clavier_Test();
	virtual void test() override;
	void test_send_input();
};
