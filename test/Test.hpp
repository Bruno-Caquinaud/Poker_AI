using namespace std;

template<class T, class X>
class Test
{
  protected :

  bool success;
  vector<vector<T>> jeu_entree;
  vector<vector<X>> jeu_sortie;
  virtual void init_jeu_entree() = 0;
  virtual void init_jeu_sortie() = 0;

  public :
  virtual void test() = 0;
  bool get_success(){return success;}
  void set_success(bool success){this->success = success;}
};
