#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/XTest.h>
#include "../../src/Interface graphique/Interface_Graphique.hpp"
#include "../Test.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"

#define NB_TEST_CASE_SEARCH_WINDOW 2
#define NB_TEST_CASE_MINIMIZE 2
#define NB_TEST_CASE_MAXIMIZE 2
#define NB_TEST_CASE_GET_IMAGE 2

using namespace std;
using namespace cv;

class Interface_Graphique_Test : public Test<Interface_Graphique, Interface_Graphique>
{
	private :

	virtual void init_jeu_entree() override;
	virtual void init_jeu_sortie() override;
	void init_jeu_entree_test_serch_window();
	void init_jeu_entree_test_minimize();
	void init_jeu_entree_test_maximize();
	void init_jeu_entree_test_get_image();
	void init_jeu_sortie_test_search_window();
	void init_jeu_sortie_test_minimize();
	void init_jeu_sortie_test_maximize();
	void init_jeu_sortie_test_get_image();
	void test_search_window();
	void test_minimize();
	void test_maximize();
	void test_get_image();

	public :

	Interface_Graphique_Test();
	virtual void test() override;
};
