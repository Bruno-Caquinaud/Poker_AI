#include "Interface_Graphique_Test.hpp"

Interface_Graphique_Test::Interface_Graphique_Test()
{
	success = false;
}

void Interface_Graphique_Test::init_jeu_entree()
{
	init_jeu_entree_test_serch_window();
	init_jeu_entree_test_minimize();
	init_jeu_entree_test_maximize();
	init_jeu_entree_test_get_image();
}

void Interface_Graphique_Test::init_jeu_sortie()
{
	init_jeu_sortie_test_search_window();
	init_jeu_sortie_test_minimize();
	init_jeu_sortie_test_maximize();
	init_jeu_sortie_test_get_image();
}

void Interface_Graphique_Test::init_jeu_entree_test_serch_window()
{
	vector<Interface_Graphique> jeu_de_donne_entree_search_window;

	for(int test_case = 0; test_case < NB_TEST_CASE_SEARCH_WINDOW; test_case++)
	{
		Interface_Graphique interface_graphique;
		Display *display;
    Window * window;

		switch(test_case)
		{
			case 0 :
				jeu_de_donne_entree_search_window.push_back(interface_graphique);
				break;
			case 1 :
				display = XOpenDisplay (NULL);
				interface_graphique.set_display(display);
				interface_graphique.set_name("bruno@bruno: ~/Documents/Poker_IA/build");
				jeu_de_donne_entree_search_window.push_back(interface_graphique);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

	jeu_entree.push_back(jeu_de_donne_entree_search_window);
}

void Interface_Graphique_Test::init_jeu_entree_test_minimize()
{
	vector<Interface_Graphique> jeu_de_donne_entree_minimize;

	for(int test_case = 0; test_case < NB_TEST_CASE_MINIMIZE; test_case++)
	{
		Interface_Graphique interface_graphique;
		Display *display;
		Window window;

		switch(test_case)
		{
			case 0 :
				jeu_de_donne_entree_minimize.push_back(interface_graphique);
				break;
			case 1 :
				display = XOpenDisplay (NULL);
				window = XDefaultRootWindow(display);
				interface_graphique.set_display(display);
				interface_graphique.set_name("bruno@bruno: ~/Documents/Poker_IA/build");
				interface_graphique.search_window(window);
				jeu_de_donne_entree_minimize.push_back(interface_graphique);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

	jeu_entree.push_back(jeu_de_donne_entree_minimize);
}

void Interface_Graphique_Test::init_jeu_entree_test_maximize()
{
	vector<Interface_Graphique> jeu_de_donne_entree_maximize;

	for(int test_case = 0; test_case < NB_TEST_CASE_MAXIMIZE; test_case++)
	{
		Interface_Graphique interface_graphique;
		Display *display;
		Window window;

		switch(test_case)
		{
			case 0 :
				jeu_de_donne_entree_maximize.push_back(interface_graphique);
				break;
			case 1 :
				display = XOpenDisplay (NULL);
				window = XDefaultRootWindow(display);
				interface_graphique.set_display(display);
				interface_graphique.set_name("bruno@bruno: ~/Documents/Poker_IA/build");
				interface_graphique.search_window(window);
				jeu_de_donne_entree_maximize.push_back(interface_graphique);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

	jeu_entree.push_back(jeu_de_donne_entree_maximize);
}

void Interface_Graphique_Test::init_jeu_entree_test_get_image()
{
	vector<Interface_Graphique> jeu_de_donne_entree_get_image;

	for(int test_case = 0; test_case < NB_TEST_CASE_GET_IMAGE; test_case++)
	{
		Interface_Graphique interface_graphique;
		Display *display;
		Window window;

		switch(test_case)
		{
			case 0 :
				jeu_de_donne_entree_get_image.push_back(interface_graphique);
				break;
			case 1 :
				display = XOpenDisplay (NULL);
				window = XDefaultRootWindow(display);
				interface_graphique.set_display(display);
				interface_graphique.set_name("bruno@bruno: ~/Documents/Poker_IA/build");
				interface_graphique.search_window(window);
				jeu_de_donne_entree_get_image.push_back(interface_graphique);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

	jeu_entree.push_back(jeu_de_donne_entree_get_image);
}

void Interface_Graphique_Test::init_jeu_sortie_test_search_window()
{

}

void Interface_Graphique_Test::init_jeu_sortie_test_minimize()
{

}

void Interface_Graphique_Test::init_jeu_sortie_test_maximize()
{

}

void Interface_Graphique_Test::init_jeu_sortie_test_get_image()
{

}

void Interface_Graphique_Test::test()
{
	init_jeu_entree();
	init_jeu_sortie();
	/*test_search_window();
	sleep(1);
	test_minimize();
	sleep(1);
	test_maximize();*/
	test_get_image();
	success = true;
}

void Interface_Graphique_Test::test_search_window()
{
  vector<Interface_Graphique> jeu_entree_search_window = jeu_entree.at(0);

  for(int i = 0; i < jeu_entree_search_window.size(); i++)
  {
    Interface_Graphique & interface_graphique = jeu_entree_search_window.at(i);
		Window root;

		if(interface_graphique.get_display()!=NULL)
    {
			root = XDefaultRootWindow(interface_graphique.get_display());
    	interface_graphique.search_window(root);
    }
  }
}

void Interface_Graphique_Test::test_minimize()
{
  vector<Interface_Graphique> jeu_entree_minimize = jeu_entree.at(1);

  for(int i = 0; i < jeu_entree_minimize.size(); i++)
  {
    Interface_Graphique & interface_graphique = jeu_entree_minimize.at(i);

		interface_graphique.minimize();
  }
}

void Interface_Graphique_Test::test_maximize()
{
	vector<Interface_Graphique> jeu_entree_maximize = jeu_entree.at(2);

	for(int i = 0; i < jeu_entree_maximize.size(); i++)
	{
		Interface_Graphique & interface_graphique = jeu_entree_maximize.at(i);

		interface_graphique.maximize();
	}
}

void Interface_Graphique_Test::test_get_image()
{
	vector<Interface_Graphique> jeu_entree_get_image = jeu_entree.at(3);

	for(int i = 0; i < jeu_entree_get_image.size(); i++)
	{
		Interface_Graphique & interface_graphique = jeu_entree_get_image.at(i);
		XImage * image;

		image = interface_graphique.get_image();

		if(image != nullptr)
		{
				unsigned long first_pixel = XGetPixel(image, 0, 0);
				char * data = image->data;
				unsigned char mat [image->height * image->width * 3]={0};
				unsigned long red_mask = 0x00FF0000;
				unsigned long green_mask = 0x0000FF00;
				unsigned long blue_mask = 0x000000FF;

				cout << "taille mat :" << image->height * image->width << endl;

				for (int x = 0; x < image->height; x++)
				{
    			for (int y = 0; y < image->width; y++)
					{
        		unsigned long pixel = XGetPixel(image, y, x);

						mat[x * image->width + y] = (pixel & blue_mask)/3;
						mat[x * image->width + y] += ((pixel & green_mask) >> 8)/3;
						mat[x * image->width + y] += ((pixel & red_mask) >> 16)/3;
					}
    		}

				Mat src =  Mat(image->height, image->width, CV_8UC1, mat);
				string image_name = "Grey Array Image";
				namedWindow(image_name, WINDOW_AUTOSIZE);
				imshow(image_name, src);
				cout << "Matric :\n" << src << endl;

				waitKey();
				destroyAllWindows();
		}
	}
}
