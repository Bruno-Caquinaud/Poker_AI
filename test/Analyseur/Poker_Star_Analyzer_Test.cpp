#include "Poker_Star_Analyzer_Test.hpp"

Poker_Star_Analyzer_Test::Poker_Star_Analyzer_Test()
{
   success = false;
}

void Poker_Star_Analyzer_Test::init_jeu_entree()
{
  init_jeu_entree_test_extract_name();
  init_jeu_entree_test_extract_money();
  init_jeu_entree_test_extract_card();
  init_jeu_entree_test_analyze();
}

void Poker_Star_Analyzer_Test::init_jeu_sortie()
{
  init_jeu_sortie_test_extract_name();
  init_jeu_sortie_test_extract_money();
  init_jeu_sortie_test_extract_card();
  init_jeu_sortie_test_analyze();
}

void Poker_Star_Analyzer_Test::init_jeu_entree_test_analyze()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_get_position_player;

	for(int test_case = 0; test_case < NB_TEST_CASE_GET_POSITION_PLAYER; test_case++)
	{
		Poker_Star_Analyzer aig;
    Mat image;

		switch(test_case)
		{
			case 0 :
        aig.set_image(image);
				jeu_de_donne_entree_get_position_player.push_back(aig);
				break;
			case 1 :
        image = imread(IMAGE_POSITION_PLAYERS, IMREAD_COLOR);
        aig.set_image(image);
				jeu_de_donne_entree_get_position_player.push_back(aig);
				break;
			default:
				cout << "Erreur Case " << test_case << " not implemented" << endl;
				break;
		}
	}

  jeu_entree.push_back(jeu_de_donne_entree_get_position_player);
}

void Poker_Star_Analyzer_Test::init_jeu_entree_test_extract_name()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_extract_name;

  for(int test_case = 0; test_case < NB_TEST_CASE_PLAYER_NAME; test_case++)
  {
    Poker_Star_Analyzer aig;
    Mat image;

    switch(test_case)
    {
      case 0 :
        aig.set_image(image);
        jeu_de_donne_entree_extract_name.push_back(aig);
        break;
      case 1 :
        image = imread(IMAGE_NAME_PLAYER, IMREAD_COLOR);
        aig.set_image(image);
        jeu_de_donne_entree_extract_name.push_back(aig);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_entree.push_back(jeu_de_donne_entree_extract_name);
}

void Poker_Star_Analyzer_Test::init_jeu_entree_test_extract_money()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_extract_money;

  for(int test_case = 0; test_case < NB_TEST_CASE_PLAYER_MONEY; test_case++)
  {
    Poker_Star_Analyzer aig;
    Mat image;

    switch(test_case)
    {
      case 0 :
        aig.set_image(image);
        jeu_de_donne_entree_extract_money.push_back(aig);
        break;
      case 1 :
        image = imread(IMAGE_MONEY_PLAYER, IMREAD_COLOR);
        aig.set_image(image);
        jeu_de_donne_entree_extract_money.push_back(aig);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_entree.push_back(jeu_de_donne_entree_extract_money);
}

void Poker_Star_Analyzer_Test::init_jeu_entree_test_extract_card()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_extract_card;

  for(int test_case = 0; test_case < NB_TEST_CASE_PLAYER_CARD; test_case++)
  {
    Poker_Star_Analyzer aig;
    Mat image;

    switch(test_case)
    {
      case 0 :
        aig.set_image(image);
        jeu_de_donne_entree_extract_card.push_back(aig);
        break;
      case 1 :
        image = imread(IMAGE_CARD_PLAYER, IMREAD_COLOR);
        aig.set_image(image);
        jeu_de_donne_entree_extract_card.push_back(aig);
        break;
        case 2 :
        image = imread(IMAGE_UNKNOW_CARD_PLAYER, IMREAD_COLOR);
        aig.set_image(image);
        jeu_de_donne_entree_extract_card.push_back(aig);
        break;
        case 3:
        image = imread(IMAGE_CARD_TABLE, IMREAD_COLOR);
        aig.set_image(image);
        jeu_de_donne_entree_extract_card.push_back(aig);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_entree.push_back(jeu_de_donne_entree_extract_card);
}


void Poker_Star_Analyzer_Test::init_jeu_sortie_test_analyze()
{
  vector<string> jeu_de_donne_sortie_get_position_player;

  for(int test_case = 0; test_case < NB_TEST_CASE_GET_POSITION_PLAYER; test_case++)
  {
    string position;

    switch(test_case)
    {
      case 0 :
        position = "";
        jeu_de_donne_sortie_get_position_player.push_back(position);
        break;
      case 1 :
        position = "";
        jeu_de_donne_sortie_get_position_player.push_back(position);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_sortie.push_back(jeu_de_donne_sortie_get_position_player);
}

void Poker_Star_Analyzer_Test::init_jeu_sortie_test_extract_name()
{
  vector<string> jeu_de_donne_sortie_extract_name;

  for(int test_case = 0; test_case < NB_TEST_CASE_PLAYER_NAME; test_case++)
  {
    string name;

    switch(test_case)
    {
      case 0 :
        name = "";
        jeu_de_donne_sortie_extract_name.push_back(name);
        break;
      case 1 :
        name = "Paladina304";
        jeu_de_donne_sortie_extract_name.push_back(name);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_sortie.push_back(jeu_de_donne_sortie_extract_name);
}

void Poker_Star_Analyzer_Test::init_jeu_sortie_test_extract_money()
{
  vector<string> jeu_de_donne_sortie_extract_money;

  for(int test_case = 0; test_case < NB_TEST_CASE_PLAYER_MONEY; test_case++)
  {
    string money;

    switch(test_case)
    {
      case 0 :
        money = "";
        jeu_de_donne_sortie_extract_money.push_back(money);
        break;
      case 1 :
        money = "1006614";
        jeu_de_donne_sortie_extract_money.push_back(money);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_sortie.push_back(jeu_de_donne_sortie_extract_money);
}

void Poker_Star_Analyzer_Test::init_jeu_sortie_test_extract_card()
{
  vector<string> jeu_de_donne_sortie_extract_card;

  for(int test_case = 0; test_case < NB_TEST_CASE_PLAYER_CARD; test_case++)
  {
    string card;

    switch(test_case)
    {
      case 0 :
        card = "";
        jeu_de_donne_sortie_extract_card.push_back(card);
        break;
      case 1 :
        card = "JD5S";
        jeu_de_donne_sortie_extract_card.push_back(card);
        break;
        case 2 :
        card = "Unknow cards\0Unknow cards\0";
        jeu_de_donne_sortie_extract_card.push_back(card);
        break;
        case 3 :
        card = " ";
        jeu_de_donne_sortie_extract_card.push_back(card);
        break;
      default:
        cout << "Erreur Case " << test_case << " not implemented" << endl;
        break;
    }
  }

  jeu_sortie.push_back(jeu_de_donne_sortie_extract_card);
}

void Poker_Star_Analyzer_Test::test()
{
  init_jeu_entree();
  init_jeu_sortie();

  //test_extract_name();
  //test_extract_money();
  test_extract_card();
  //test_analyze();

  success = true;
}

void Poker_Star_Analyzer_Test::test_analyze()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_get_position_player = jeu_entree.at(3);

  for(int i = 0; i < jeu_de_donne_entree_get_position_player.size(); i++)
  {
    Poker_Star_Analyzer & aig = jeu_de_donne_entree_get_position_player.at(i);

    aig.analyze();
  }
}

void Poker_Star_Analyzer_Test::test_extract_name()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_extract_name = jeu_entree.at(0);
  vector<string> jeu_de_donne_sortie_extract_name = jeu_sortie.at(0);

  for(int i = 0; i < jeu_de_donne_entree_extract_name.size(); i++)
  {
    Poker_Star_Analyzer & aig = jeu_de_donne_entree_extract_name.at(i);
    string & correct_name = jeu_de_donne_sortie_extract_name.at(i);
    string current_name;
    Rect zone(0,0, aig.get_image().cols, aig.get_image().rows);

    aig.extract_name(current_name, zone);

    if(current_name == correct_name)
    {
      success = true;
    }
  }
}

void Poker_Star_Analyzer_Test::test_extract_money()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_extract_money = jeu_entree.at(1);
  vector<string> jeu_de_donne_sortie_extract_money = jeu_sortie.at(1);

  for(int i = 0; i < jeu_de_donne_entree_extract_money.size(); i++)
  {
    Poker_Star_Analyzer & aig = jeu_de_donne_entree_extract_money.at(i);
    string & correct_money = jeu_de_donne_sortie_extract_money.at(i);
    string current_money;
    Rect zone(0,0, aig.get_image().cols, aig.get_image().rows);

    aig.extract_money(current_money, zone);

    if(current_money == correct_money)
    {
      success = true;
    }
  }
}

void Poker_Star_Analyzer_Test::test_extract_card()
{
  vector<Poker_Star_Analyzer> jeu_de_donne_entree_extract_card = jeu_entree.at(2);
  vector<string> jeu_de_donne_sortie_extract_card = jeu_sortie.at(2);

  for(int i = 0; i < jeu_de_donne_entree_extract_card.size(); i++)
  {
    Poker_Star_Analyzer & aig = jeu_de_donne_entree_extract_card.at(i);
    string & correct_card = jeu_de_donne_sortie_extract_card.at(i);
    string current_card;
    Rect zone(0,0, aig.get_image().cols, aig.get_image().rows);

    aig.extract_card(current_card, zone);

    if(current_card == correct_card)
    {
      success = true;
    }
  }
}
