#include <string>
#include "../../src/Analyseur/Poker_Star_Analyzer.hpp"
#include "../Test.hpp"

#define NB_TEST_CASE_GET_POSITION_PLAYER 2
#define NB_TEST_CASE_PLAYER_NAME 2
#define NB_TEST_CASE_PLAYER_MONEY 2
#define NB_TEST_CASE_PLAYER_CARD 4
#define IMAGE_POSITION_PLAYERS "game.jpeg"
#define IMAGE_NAME_PLAYER "name.jpeg"
#define IMAGE_MONEY_PLAYER "money.jpg"
#define IMAGE_CARD_PLAYER "card.jpg"
#define IMAGE_UNKNOW_CARD_PLAYER "unkow_card.jpg"
#define IMAGE_CARD_TABLE "river.jpg"

using namespace std;

class Poker_Star_Analyzer_Test : public Test<Poker_Star_Analyzer, string>
{
	private :

	virtual void init_jeu_entree() override;
	virtual void init_jeu_sortie() override;
	void init_jeu_entree_test_analyze();
	void init_jeu_entree_test_extract_name();
	void init_jeu_entree_test_extract_money();
	void init_jeu_entree_test_extract_card();
	void init_jeu_sortie_test_analyze();
	void init_jeu_sortie_test_extract_name();
	void init_jeu_sortie_test_extract_money();
	void init_jeu_sortie_test_extract_card();

	public :

	Poker_Star_Analyzer_Test();
	virtual void test() override;
	void test_analyze();
	void test_extract_name();
	void test_extract_money();
	void test_extract_card();
};
